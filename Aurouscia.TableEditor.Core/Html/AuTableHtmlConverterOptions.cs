namespace Aurouscia.TableEditor.Core.Html
{
    public class AuTableHtmlConverterOptions
    {
        /// <summary>
        /// 单元格转换规则，会分别作用于每个单元格，返回单元格内的替换
        /// </summary>
        public Func<string?, string>? CellConverter;
        /// <summary>
        /// 单元格转换规则，会分别作用于每个单元格，返回单元格内的替换和td标签的attribute
        /// </summary>
        public Func<string?, (string tdContent, string tdAttribute)>? CellConverterAttr;
    }
}