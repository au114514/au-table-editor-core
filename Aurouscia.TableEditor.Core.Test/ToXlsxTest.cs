﻿using Aurouscia.TableEditor.Core.Excel;

namespace Aurouscia.TableEditor.Core.Test
{
    [TestClass]
    public class ToXlsxTest
    {
        [TestMethod]
        public void Common()
        {
            var row1 = new List<string?> { "0A", "0B" };
            var row2 = new List<string?> { "1A", "1B" };
            AuTable table = new()
            {
                Cells = new(){ row1, row2 },
            };
            Stream? s = table.ToXlsx(out _);
            Assert.IsNotNull(s);
            var table2 = AuTableExcelConverter.FromXlsx(s, out _);
            Assert.IsNotNull(table2);
            Assert.IsNotNull(table2.Cells);
            CollectionAssert.AreEqual(table.Cells[0], table2.Cells[0]);
            CollectionAssert.AreEqual(table.Cells[1], table2.Cells[1]);
        }
        [TestMethod]
        public void Merged()
        {
            AuTable table = new()
            {
                Cells = new()
                {
                    new() { "0A", "0B", "0C" },
                    new() { "1A", "1B", "1C" },
                    new() { "2A", "2B", "2C" }
                },
                Merges = new List<AuTableMergeInfo?>
                {
                    new()
                    {
                        Row = 0,
                        Col = 1,
                        Right = 1
                    },
                    new()
                    {
                        Row = 1,
                        Col = 0,
                        Right = 1,
                        Down = 1,
                    }
                }
            };
            Stream? s = table.ToXlsx(out _);
            Assert.IsNotNull(s);
            var table2 = AuTableExcelConverter.FromXlsx(s, out _);
            Assert.IsNotNull(table2);
            Assert.IsNotNull(table2.Cells);
            CollectionAssert.AreEqual(table.Cells[0], table2.Cells[0]);
            CollectionAssert.AreEqual(table.Cells[1], table2.Cells[1]);
            CollectionAssert.AreEqual(table.Cells[2], table2.Cells[2]);
            Assert.IsNotNull(table2.Merges);
            foreach (var m in table.Merges)
            {
                if (m is null) continue;
                var sameCount = table2.Merges.Count(x => x is not null
                    && x.Row == m.Row && x.Col == m.Col
                    && x.Right == m.Right && x.Down == m.Down);
                Assert.AreEqual(1, sameCount);
            }
        }
    }
}
