using Aurouscia.TableEditor.Core.Html;

namespace Aurouscia.TableEditor.Core.Test
{
    [TestClass]
    public class ToHtmlTest
    {
        [TestMethod]
        public void Common()
        {
            AuTable table = new()
            {
                Cells = new()
                {
                    new() { "0A", "0B" },
                    new() { "1A", "1B" },
                },
            };
            string answer = 
                "<table>" +
                "<tr><td>0A</td><td>0B</td></tr>" +
                "<tr><td>1A</td><td>1B</td></tr>" +
                "</table>";
            Assert.AreEqual(answer, table.ConvertToHtml());
        }
        [TestMethod]
        public void Merged()
        {
            AuTable table = new()
            {
                Cells = new()
                {
                    new() { "0A", "0B", "0C" },
                    new() { "1A", "1B", "1C" },
                    new() { "2A", "2B", "2C" }
                },
                Merges = new List<AuTableMergeInfo?>
                {
                    new()
                    {
                        Row = 0,
                        Col = 1,
                        Right = 1
                    },
                    new()
                    {
                        Row = 1,
                        Col = 0,
                        Right = 1,
                        Down = 1,
                    }
                }
            };
            string answer = 
                "<table>" +
                "<tr><td>0A</td><td colspan=\"2\">0B</td></tr>" +
                "<tr><td rowspan=\"2\" colspan=\"2\">1A</td><td>1C</td></tr>" +
                "<tr><td>2C</td></tr>" +
                "</table>";
            Assert.AreEqual(answer, table.ConvertToHtml());
        }

        [TestMethod]
        public void Colored()
        {
            AuTable table = new()
            {
                Cells = new()
                {
                    new() { "0A", "0B", "0C" },
                    new() { "1A", "1B", "1C" },
                    new() { "2A", "2B", "2C" }
                },
                Merges = null,
                Colors = [
                    new AuTableCellColorInfo(){
                        Pos = [0,0],
                        Bg = "#ff0000"
                    },
                    new AuTableCellColorInfo(){
                        Pos = [1,1],
                        Text = "#00ff00"
                    },
                    new AuTableCellColorInfo()
                    {
                        Pos = [2,2],
                        Text = "#ffff00",
                        Bg = "#00ffff"
                    }
                ]
            };
            string answer = 
                "<table>" +
                "<tr><td style=\"background-color:#ff0000;\">0A</td><td>0B</td><td>0C</td></tr>" +
                "<tr><td>1A</td><td style=\"color:#00ff00;\">1B</td><td>1C</td></tr>" +
                "<tr><td>2A</td><td>2B</td><td style=\"color:#ffff00;background-color:#00ffff;\">2C</td></tr>" +
                "</table>";
            Assert.AreEqual(answer, table.ConvertToHtml());
        }
        [TestMethod]
        public void Convert()
        {
            AuTable table = new()
            {
                Cells = new()
                {
                    new() { "0A", "0*B" },
                    new() { "1*A", "1B" },
                },
            };
            AuTableHtmlConverterOptions options = new AuTableHtmlConverterOptions()
            {
                CellConverter = (s) =>
                {
                    if (s is null) return "";
                    return s.Replace("*", "^");
                }
            };
            string answer =
                "<table>" +
                "<tr><td>0A</td><td>0^B</td></tr>" +
                "<tr><td>1^A</td><td>1B</td></tr>" +
                "</table>";
            Assert.AreEqual(answer, table.ConvertToHtml(options));
        }
        [TestMethod]
        public void ConvertAttr()
        {
            AuTable table = new()
            {
                Cells = new()
                {
                    new() { "0A", "0*B" },
                    new() { "1*A", "" },
                },
                Merges = [
                    new()
                    {
                        Row = 0, 
                        Col = 1,
                        Down = 1
                    }
                ]
            };
            AuTableHtmlConverterOptions options = new AuTableHtmlConverterOptions()
            {
                //如果传入了CellConverterAttr，将会忽略CellConverter
                CellConverter = (s) =>
                {
                    if (s is null) return "";
                    return s.Replace("*", "^");
                },
                CellConverterAttr = (s) =>
                {
                    if (s is null)
                        return ("", "");
                    if (s.StartsWith("0"))
                        return (s.Substring(1), "class=\"startWithZero\"");
                    return (s, "");
                }
            };
            string answer =
                "<table>" +
                "<tr>" +
                "<td class=\"startWithZero\">A</td>" +
                "<td rowspan=\"2\" class=\"startWithZero\">*B</td></tr>" +
                "<tr><td>1*A</td></tr>" +
                "</table>";
            Assert.AreEqual(answer, table.ConvertToHtml(options));
        }
        [TestMethod]
        public void Sticky()
        {
            AuTable table = new()
            {
                Cells = new()
                {
                    new() { "0A", "0B", "0C" },
                    new() { "1A", "1B", "1C" },
                    new() { "2A", "2B", "2C" },
                    new() { "3A", "3B", "3C" },
                },
                Sticky = new()
                {
                    LeftIdx = 1,
                    TopIdx = 2,
                    LeftFromRowIdx = 2,
                    TopFromColIdx = 0
                }
            };
            string sca = AuTableHtmlConvertExtension.stickyContainAttr;
            string sta = AuTableHtmlConvertExtension.stickyTopAttr;
            string sla = AuTableHtmlConvertExtension.stickyLeftAttr;
            string answer =
                $"<table {sca}>" +
                $"<tr><td>0A</td><td>0B</td><td>0C</td></tr>" +
                $"<tr><td {sla} {sta}>1A</td><td {sta}>1B</td><td {sta}>1C</td></tr>" +
                $"<tr><td {sla}>2A</td><td>2B</td><td>2C</td></tr>" +
                $"<tr><td {sla}>3A</td><td>3B</td><td>3C</td></tr>" +
                "</table>";
            Assert.AreEqual(answer, table.ConvertToHtml());
        }
    }
}