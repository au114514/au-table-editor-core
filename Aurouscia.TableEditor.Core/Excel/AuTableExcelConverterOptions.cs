﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurouscia.TableEditor.Core.Excel
{
    public class AuTableExcelConverterOptions
    {
        /// <summary>
        /// 移除底部的空行<br/>
        /// （当单元格有背景色，边框等样式信息时，会把表格边界拉大，如果不希望末尾出现大量空单元格将此项设为true）
        /// </summary>
        public bool RemoveTrailingEmptyRows { get; set; } = false;
        /// <summary>
        /// 移除右侧的空列<br/>
        /// （当单元格有背景色，边框等样式信息时，会把表格边界拉大，如果不希望末尾出现大量空单元格将此项设为true）
        /// </summary>
        public bool RemoveTrailingEmptyColumns { get; set; } = false;
        public int RowCountLimit { get; set; } = int.MaxValue;
        public int ColumnCountLimit { get; set; } = int.MaxValue;
    }
}
