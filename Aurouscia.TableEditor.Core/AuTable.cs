﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurouscia.TableEditor.Core
{
    public class AuTable
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public List<List<string?>?>? Cells { get; set; }
        public List<AuTableMergeInfo?>? Merges { get; set; }
        public List<AuTableCellColorInfo?>? Colors { get; set; }
        public AuTableStickyInfo? Sticky { get; set; }
    }
    public class AuTableMergeInfo
    {
        public int Row { get; set; }
        public int Col { get; set; }
        public int Down { get; set; }
        public int Right { get; set; }
    }
    public class AuTableCellColorInfo
    {
        public int[]? Pos { get; set; }
        public string? Bg { get; set; }
        public string? Text { get; set; }

        public bool Match(int row, int col)
        {
            if (Pos is { } && Pos.Length == 2)
                return row == Pos[0] && col == Pos[1];
            return false;
        }
    }
    public class AuTableStickyInfo
    {
        //均为1开始的index
        public int TopIdx { get; set; }
        public int TopFromColIdx { get; set; }
        public int LeftIdx { get; set; }
        public int LeftFromRowIdx { get; set; }
    }
    public struct CellPos
    {
        public int Row { get; }
        public int Col { get; }
        public CellPos() { }
        public CellPos(int row,int col)
        {
            Row = row;
            Col = col;
        }
    }
}
