﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aurouscia.TableEditor.Core.Utils
{
    public static class AuTableUtilsExtension
    {
        public static int GetRowCount(this AuTable table)
        {
            return table.Cells is null ? 0 : table.Cells.Count;
        }
        public static int GetColumnCount(this AuTable table)
        {
            if (table.Cells is null) return 0;
            if (table.Cells.Count == 0) return 0;
            var longestRow = table.Cells.MaxBy(x => x is null ? 0 : x.Count);
            if (longestRow is not null)
                return longestRow.Count;
            return 0;
        }
        public static void FillToRect(this AuTable table)
        {
            if (table.Cells is null) return;
            int colCount = table.GetColumnCount();
            int rowCount = table.GetRowCount();
            for (int i = 0; i < rowCount; i++)
            {
                var row = table.Cells[i];
                if (row is null)
                {
                    table.Cells[i] = EmptyStringList(colCount);
                }
                else if (row.Count < colCount)
                {
                    int sub = colCount - row.Count;
                    row.AddRange(EmptyStringList(sub));
                }
            }
        }
        private static List<string?> EmptyStringList(int length)
        {
            List<string?> res = new(length);
            for(int i=0; i < length; i++)
            {
                res.Add(string.Empty);
            }
            return res;
        }
    }
}
