namespace Aurouscia.TableEditor.Core.Utils
{
    public static class ColorUtil
    {
        public static string? ToHex(params int[] rgb)
        {
            if (rgb.Length != 3)
                return null;
            return string.Format("#{0:x2}{1:x2}{2:x2}", rgb[0], rgb[1], rgb[2]);
        }

        public static bool IsBlack(params int[] rgb)
        {
            return rgb[0] == 0 && rgb[1] == 0 && rgb[2] == 0;
        }
    }
}