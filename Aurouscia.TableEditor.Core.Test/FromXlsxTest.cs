﻿using Aurouscia.TableEditor.Core.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aurouscia.TableEditor.Core.Utils;

namespace Aurouscia.TableEditor.Core.Test
{
    [TestClass]
    public class FromXlsxTest
    {
        [TestMethod]
        public void Common()
        {
            FileStream fs = File.OpenRead("./Data/FromXlsx_Common.xlsx");
            var data = AuTableExcelConverter.FromXlsx(fs, out _);
            var answerCellsRow1 = new List<string>() { "1A", "1B", "1C" };
            var answerCellsRow2 = new List<string>() { "2A", "666", "" };
            var answerCellsRow3 = new List<string>() { "", "2024/01/30", "" };
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Cells);
            CollectionAssert.AreEqual(answerCellsRow1, data.Cells[0]);
            CollectionAssert.AreEqual(answerCellsRow2, data.Cells[1]);
            CollectionAssert.AreEqual(answerCellsRow3, data.Cells[2]);
        }
        [TestMethod]
        public void Merged()
        {
            FileStream fs = File.OpenRead("./Data/FromXlsx_Merged.xlsx");
            var data = AuTableExcelConverter.FromXlsx(fs, out _);
            var answerCellsRow1 = new List<string>() { "1A", "1B", "1C", "" };
            var answerCellsRow2 = new List<string>() { "2A", "", "", "" };
            var answerCellsRow3 = new List<string>() { "AAA", "", "","" };
            var mergeInfo = new List<AuTableMergeInfo>()
            {
                new AuTableMergeInfo(){ Row = 0, Col=1, Down=1},
                new AuTableMergeInfo(){ Row = 2, Col=0, Right=1},
                new AuTableMergeInfo(){ Row = 1, Col=2, Down=1, Right=1 }
            };
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Cells);
            CollectionAssert.AreEqual(answerCellsRow1, data.Cells[0]);
            CollectionAssert.AreEqual(answerCellsRow2, data.Cells[1]);
            CollectionAssert.AreEqual(answerCellsRow3, data.Cells[2]);
            Assert.IsNotNull(data.Merges);
            foreach(var m in mergeInfo)
            {
                var sameCount = data.Merges.Count(x => x is not null 
                    && x.Row == m.Row && x.Col == m.Col 
                    && x.Right == m.Right && x.Down == m.Down);
                Assert.AreEqual(1, sameCount);
            }
        }
        
        [TestMethod]
        [DataRow(2, 10, false)]
        [DataRow(3, 10, true)]
        [DataRow(10, 2, false)]
        [DataRow(10, 3, true)]
        [DataRow(10, 10, true)]
        [DataRow(2, 2, false)]
        public void RowColCountLimit(int rowLimit, int colLimit, bool expectSuccess)
        {
            FileStream fs = File.OpenRead("./Data/FromXlsx_Common.xlsx");
            var data = AuTableExcelConverter.FromXlsx(fs, new()
            {
                RowCountLimit = rowLimit,
                ColumnCountLimit = colLimit
            }, out _);
            var success = data != null;
            Assert.AreEqual(expectSuccess, success);
        }

        [TestMethod]
        [DataRow(false, false, 5, 4)]
        [DataRow(false, true, 5, 2)]
        [DataRow(true, false, 3, 4)]
        [DataRow(true, true, 3, 2)]
        public void RemoveTrailingEmptyCells(bool rmTrailingEmptyRow, bool rmTrailingEmptyCol, int expectedRowCount, int expectedColCount)
        {
            FileStream fs = File.OpenRead("./Data/FromXlsx_EmptyCells.xlsx");
            var data = AuTableExcelConverter.FromXlsx(fs, new()
            {
                RemoveTrailingEmptyColumns = rmTrailingEmptyCol,
                RemoveTrailingEmptyRows = rmTrailingEmptyRow
            }, out _);
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Cells);
            Assert.AreEqual(expectedRowCount, data.GetRowCount());
            Assert.AreEqual(expectedColCount, data.GetColumnCount());

            List<string> r1 = ["123", "456"];
            List<string> r2 = ["嘿嘿嘿", ""];
            List<string> r3 = ["789", ""];
            CollectionAssert.AreEqual(r1, data.Cells[0]!.Slice(0, 2));
            CollectionAssert.AreEqual(r2, data.Cells[1]!.Slice(0, 2));
            CollectionAssert.AreEqual(r3, data.Cells[2]!.Slice(0, 2));
        }

        [TestMethod]
        public void EvaluateFormulas()
        {
            FileStream fs = File.OpenRead("./Data/FromXlsx_Formula.xlsx");
            var data = AuTableExcelConverter.FromXlsx(fs, out _);
            Assert.IsNotNull(data);
            Assert.IsNotNull(data.Cells);
            Assert.AreEqual("18", data.Cells[4]![1]);
            Assert.AreEqual("112.33", data.Cells[4]![2]);
        }

        [TestMethod]
        public void ReadColorStyles()
        {
            FileStream fs = File.OpenRead("./Data/FromXlsx_Colors.xlsx");
            var data = AuTableExcelConverter.FromXlsx(fs, out _);
            Assert.IsNotNull(data);
            CollectionAssert.AreEqual(new List<string>(){"A1","B1","C1"}, data.Cells![0]);
            CollectionAssert.AreEqual(new List<string>(){"A2","B2","C2"}, data.Cells![1]);
            Assert.IsNotNull(data.Colors);
            Assert.AreEqual(4, data.Colors.Count);
            Assert.IsNotNull(data.Colors.Find(c =>
                c is not null && c.Pos![0]==0 && c.Pos![1]==1 && c.Text is null && c.Bg == "#ffff00"));
            Assert.IsNotNull(data.Colors.Find(c =>
                c is not null && c.Pos![0]==0 && c.Pos![1]==2 && c.Text=="#ff0000" && c.Bg is null));
            Assert.IsNotNull(data.Colors.Find(c =>
                c is not null && c.Pos![0]==1 && c.Pos![1]==0 && c.Text=="#ffffff" && c.Bg=="#5b9bd5"));
            Assert.IsNotNull(data.Colors.Find(c =>
                c is not null && c.Pos![0]==1 && c.Pos![1]==1 && c.Text=="#00b050" && c.Bg=="#000000"));
        }
    }
}
